package com.company.service;

public class Security {
    private String name;
    private boolean free;
    private String department;

    public Security(String name, boolean free, String department) {
        this.name = name;
        this.free = free;
        this.department = department;
    }

    public void openDoor(){

    }
    public void closeDoor(){

    }
    public void checkVisitor() {

    }

    public boolean isFree() {
        return free;
    }
}
