package com.company.goods;

public class HardDrive {
    private String name;
    private boolean hasGuarantee;
    private String volume;
    private String department;
    private String price;
    private String company;

    public HardDrive(String name, boolean hasGuarantee, String volume, String department, String price, String company) {
        this.name = name;
        this.hasGuarantee = hasGuarantee;
        this.volume = volume;
        this.department = department;
        this.price = price;
        this.company = company;
    }

    public void format() {

    }
    public void copy(){

    }

    public boolean isHasGuarantee() {
        return hasGuarantee;
    }

    public void delete(){

    }
}
