package com.company.goods;

public class Televisor {
    private String name;
    private boolean hasGuarantee;
    private String company;
    private String department;
    private String price;
    private String model;

    public Televisor(String name, boolean hasGuarantee, String company, String department, String price, String model) {
        this.name = name;
        this.hasGuarantee = hasGuarantee;
        this.company = company;
        this.department = department;
        this.price = price;
        this.model = model;
    }

    public void on(){

    }
    public void off(){

    }
    public void selectChannel(){

    }

    public boolean isHasGuarantee() {
        return hasGuarantee;
    }
}
